// Знайти всі параграфи на сторінці та встановити колір фону #ff0000

// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

// Встановіть в якості контента елемента з класом testParagraph наступний параграф -
// This is a paragraph

// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

"use strict";

// First task
document
  .querySelectorAll("p")
  .forEach((element) => (element.style.backgroundColor = "#ff0000"));

//   Second task
console.log(document.querySelector("#optionsList"));

console.log(document.querySelector("#optionsList").parentElement);

document
  .querySelector("#optionsList")
  .childNodes.forEach((node) =>
    console.log(node["nodeName"], node["nodeType"])
  );

//   Third task
// !Since there is no element with the "testParagraph" class, I used the element with the "testParagraph" id
document.querySelector("#testParagraph").innerHTML =
  "<p>This is a paragraph</p>";

// Fourth task
let headerChildrens = document.querySelector(".main-header").children;
console.log(headerChildrens);

for (let child of headerChildrens) {
  child.classList.add("nav-item");
}

// Fifth task
document
  .querySelectorAll(".section-title")
  .forEach((element) => element.classList.remove("section-title"));
